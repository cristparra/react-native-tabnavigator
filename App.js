/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';

import { createStackNavigator } from 'react-navigation';
import{createBottomTabNavigator}from 'react-navigation'
import Explore from './screens/Explore'
import Inbox from './screens/Inbox'
import Saved from './screens/Saved'
import User from './screens/User'

 

export default createBottomTabNavigator({
  Explore:{
    screen:Explore,
    navigationOptions:{
      tabBarLabel: 'EXPLORE',
      tabBarIcon: ({tintColor}) =>(
       <Image source={require('./assets/lupa.png')} style={{height:24,width:24,tintColor:tintColor}} />
      )
    }
  },
  Saved:{
    screen:Saved,
    navigationOptions:{
      tabBarLabel: 'SAVED',
      tabBarIcon: ({tintColor}) =>(
        <Image source={require('./assets/like.png')} style={{height:24,width:24,tintColor:tintColor}} />
      )
    }
  },
  User:{
    screen:User,
    navigationOptions:{
      tabBarLabel: 'USER',
      tabBarIcon: ({tintColor}) =>(
        <Image source={require('./assets/user.png')} style={{height:24,width:24,tintColor:tintColor}} />
      )
    }
  },
  Inbox:{
    screen:Inbox,
    navigationOptions:{
      tabBarLabel: 'INBOX',
      tabBarIcon: ({tintColor}) =>(
        <Image source={require('./assets/inbox.png')} style={{height:24,width:24,tintColor:tintColor}} />
      )
    }
  }
},
{
  tabBarOptions:{
    activeTintColor:'red',
    inacticeTintColor:'grey',
    style:{
      backgroundColor:'white',
      borderTopWidth:0,
      shadowOffset:{width:5,height:3},
      shadowColor:'black',
      shadowOpacity:0.5,
      elevation:5
    
    }
  }
}
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
