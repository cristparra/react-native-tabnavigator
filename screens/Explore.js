import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TextInput,
  Image
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome'


export default class Explore extends Component {
  render() {
    return (
      <SafeAreaView style={{flex:1}}>
        <View style={{flex:1}}>
          <View style={{height:80, backgroundColor:'white',borderBottomWidth:1, borderBottomColor:'#dddddd'}}>
            <View style={{flexDirection:'row',
                          justifyContent: 'space-between',
                          padding:20, backgroundColor:'white', marginHorizontal:10,
                          shadowOffset:{width:0,height:0},
                          shadowColor:'black',
                          shadowOpacity:0.2,
                          elevation:1
                          }}>
              <Icon  name="medkit" size={28} />
              <TextInput
              underlineColorAndroid="transparent"
                placeholder="Buscar atención médica"
                placeholderTextColor='grey'
                style={{flex:1, fontWeight:'700',backgroundColor:'white'}}
                />
            </View>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

 

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  
});
